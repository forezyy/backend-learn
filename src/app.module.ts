import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'static/swagger'),
      serveRoot: process.env.NODE_ENV === 'development' ? '/' : '/docs',
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
