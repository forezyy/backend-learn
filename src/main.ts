require('dotenv').config();

import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { createWriteStream } from 'fs';
import { get } from 'http';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('TESPEN API Documention')
    .setDescription('TESPEN API Documention')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/docs', app, document);
  await app.listen(3000);

  if (process.env.NODE_ENV === 'development') {
    var serverUrl = process.env.EZY_SERVER_URL;
    get(`${serverUrl}/docs/swagger-ui-bundle.js`, function (response) {
      response.pipe(createWriteStream('static/swagger/swagger-ui-bundle.js'));
      console.log(
        `Swagger UI bundle file written to: '/static/swagger/swagger-ui-bundle.js'`,
      );
    });

    get(`${serverUrl}/docs/swagger-ui-init.js`, function (response) {
      response.pipe(createWriteStream('static/swagger/swagger-ui-init.js'));
      console.log(
        `Swagger UI init file written to: '/static/swagger/swagger-ui-init.js'`,
      );
    });

    get(
      `${serverUrl}/docs/swagger-ui-standalone-preset.js`,
      function (response) {
        response.pipe(
          createWriteStream('static/swagger/swagger-ui-standalone-preset.js'),
        );
        console.log(
          `Swagger UI standalone preset file written to: '/static/swagger/swagger-ui-standalone-preset.js'`,
        );
      },
    );

    get(`${serverUrl}/docs/swagger-ui.css`, function (response) {
      response.pipe(createWriteStream('static/swagger/swagger-ui.css'));
      console.log(
        `Swagger UI css file written to: '/static/swagger/swagger-ui.css'`,
      );
    });
  }
}
bootstrap();
